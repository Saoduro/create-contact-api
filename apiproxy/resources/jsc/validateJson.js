var schemaContent = context.getVariable('schema');
var bodyContent = context.getVariable('request.content');
var regex = new RegExp(/^[a-zA-Z0-9+]+([\._-][a-zA-Z0-9]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9]+){0,4}\.[a-zA-Z0-9]{1,4}$/);


function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
if(IsJsonString(bodyContent)){
    var body = JSON.parse(bodyContent);
    var schema = JSON.parse(schemaContent);
    var valid = tv4.validate(body, schema, false);
    var email = body.email;
    context.setVariable("email", email);
    if(valid){
        if(typeof email != 'undefined' && !regex.test(email)) {
            context.setVariable("schemaValidationResult", "false");
            context.setVariable("schemaValidationErrorMessage", "Please provide the valid email");
        }
        else {
            context.setVariable("schemaValidationResult", "true");
            context.setVariable("schemaOut", schema);
            
        }
        
    } else {
        context.setVariable("schemaValidationResult", "false");
        context.setVariable("schemaValidationErrorMessage", tv4.error.message);
        context.setVariable("schemaValidationPath", tv4.error.schemaPath);
    }
} else {
    context.setVariable("schemaValidationResult", "false");
    context.setVariable("schemaValidationErrorMessage", "Invalid JSON");
}

